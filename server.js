var express = require('express');
var path = require('path');
var app = express();


app.use(express.static(__dirname + '/client')); 
// Catch all other routes and return the index file
// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'client/index.html'));
});

app.listen(process.env.PORT || 3000);