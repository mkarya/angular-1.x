mainApp.controller("postController", function($scope, httpService, $rootScope) {
    if(!$scope.posts){
        httpService.get('https://jsonplaceholder.typicode.com/posts').then(function(response){
            $scope.posts = response.data.slice(0,20);  
        }, function(err){
            throw new Error('Some err');
        })
    }
   
    $rootScope.$on('postAdded', function(event, payload){         
        $scope.posts.splice(0,0,{userId:payload.userId, title:payload.title, body:payload.body});        
    })
 });

 mainApp.controller("commentsController", function($scope, commentList) {     
    $scope.commentList = commentList;
 });
 
 mainApp.controller("addPostController", function($scope, $rootScope, $state) {
    $scope.submitFrom = function(e){
        var payload = {
            userId : $scope.userId,
            title: $scope.title,
            body: $scope.body
        }
        $state.go('post');
        $rootScope.$emit("postAdded", payload);
    }
 });