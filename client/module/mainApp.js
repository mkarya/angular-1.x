var mainApp = angular.module("mainApp", ["ui.router"]);
mainApp.config(function($stateProvider, $urlRouterProvider, $locationProvider){
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/');
    // $routeProvider.caseInsensitiveMatch = true;
    
   $stateProvider
   .state('post', {
    url: '/',
    templateUrl: './views/postList.html',
    controller: 'postController'
   })
   .state('comments', {
    url: '/comments/:Id',
    templateUrl: './views/postComments.html',
    controller: 'commentsController',
    resolve: {
        commentList: function($stateParams, httpService){
            var id = $stateParams.Id;
            var URL = 'https://jsonplaceholder.typicode.com/posts/'+id+'/comments';

            return   httpService.get(URL).then(function(response){
                 return response.data; 
            }, function(err){
                 throw new Error('Some err');
            })
        }
    }
   })
   .state('newpost', {
    url: '/add-post',
    templateUrl: './views/addPost.html',
    controller: 'addPostController'
   });
   
   
});


