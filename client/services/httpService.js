mainApp.factory('httpService', function($http){
    var myService = {}
    myService.get = function(url){
        return $http.get(url);
    }

    myService.post = function(url, data){
        return $http.post(url, data);
    }
    return myService;
});